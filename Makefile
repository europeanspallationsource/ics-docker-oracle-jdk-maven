.PHONY: help build-all tag-all push-all refresh-all release-all

OWNER := europeanspallationsource
# Use oracle-jdk version as image tag
TAG := 9.0.1
ALL_IMAGES := oracle-jdk \
	oracle-jdk-maven \
	oracle-jdk-maven-jenkins


help:
# http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@echo "ics-docker-oracle-jdk-maven"
	@echo "==========================="
	@echo "Replace % with a stack directory name (e.g., make build/oracle-jdk-maven)"
	@echo
	@grep -E '^[a-zA-Z0-9_%/-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build/%: ## build the latest image for a stack
	docker build -t $(OWNER)/$(notdir $@):latest ./$(notdir $@)

build-all: $(ALL_IMAGES:%=build/%) ## build all stacks

tag/%: ## tag the latest stack image with the tag
	docker tag -f $(OWNER)/$(notdir $@):latest $(OWNER)/$(notdir $@):$(TAG)

tag-all: $(ALL_IMAGES:%=tag/%) ## tag all stacks

push/%: ## push the latest and tag for a stack to Docker Hub
	docker push $(OWNER)/$(notdir $@):$(TAG)
	docker push $(OWNER)/$(notdir $@):latest

push-all: $(ALL_IMAGES:%=push/%) ## push all stacks

refresh/%: ## pull the latest image from Docker Hub for a stack
# skip if error: a stack might not be on dockerhub yet
	-docker pull $(OWNER)/$(notdir $@):latest

refresh-all: $(ALL_IMAGES:%=refresh/%) ## refresh all stacks and centos
	docker pull centos:7

release-all: refresh-all \
	build-all \
	tag-all \
	push-all
release-all: ## build, tag, and push all stacks
