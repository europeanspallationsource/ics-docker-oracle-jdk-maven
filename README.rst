Repository to build oracle-jdk and maven images
===============================================

This repository contains 3 Dockerfiles:

- oracle-jdk (install oracle-jdk)
- oracle-jdk-maven (add maven)
- oracle-jdk-maven-jenkins (add jenkins users)


Usage
-----

To be able to push the image to Docker_ Hub, you first have to login::

    $ docker login

You can then run::

    $ make release-all

This will create and push the images:

  - europeanspallationsource/oracle-jdk
  - europeanspallationsource/oracle-jdk-maven
  - europeanspallationsource/oracle-jdk-maven-jenkins

The image is tagged with both `latest` and the tag defined in the Makefile.
We use the version of oracle-jdk as tag.
Change the Makefile if you update it!


.. _Docker: https://www.docker.com
